import { PostCard } from "../postCard"
import "./styles.css"

export const AllPosts = ({posts}) =>{

    

    return (
        <div className="posts">
            {posts.map(post => 
            (
                <PostCard post={post}  key={post.id}/>
            ))}
      </div>
    )
}