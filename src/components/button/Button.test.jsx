import { fireEvent, render, screen } from "@testing-library/react"
import { Button } from "."

describe("<Button />",()=>{

    it("botao esta em tela ?", ()=>{
        render(<Button text="load more"/>)

        const button = screen.getByRole('button',{name: /load more/i} )
        expect(button).toBeInTheDocument()
    });    


    it("Ao clicar chama a funcao ?", ()=>{

        const fn = jest.fn()
        render(<Button text="load more" onClick={fn()}/>)
        const button = screen.getByRole('button',{name: /load more/i} )

        fireEvent.click(button)

        expect(fn).toHaveBeenCalledTimes(1)
        
    })    
    
    it("o botao esta desativando ?", ()=>{

        render(<Button text="load more" disabledOrNot={true}/>)
        const button = screen.getByRole('button',{name: /load more/i} );
        expect(button).toBeDisabled()
        
    })
})