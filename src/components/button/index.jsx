import { Component } from "react";
import "./styles.css"

export class Button extends Component{



    render(){
        const {text, onClick, disabledOrNot} = this.props

        return (
            <button className="button" onClick={onClick} disabled={disabledOrNot}>{text}</button>
        )
    }
}