import "./styles.css"

export const TextInput = ({searchValue, handleChange}) => { 
    

    return( 
        <>
            {!!searchValue&& <h1 className="input-text">pesquisando por:  {searchValue} </h1>}
            <input className="input" type="search" placeholder=" digite sua pesquisa"  onChange={handleChange}/>
        </>
        )
}