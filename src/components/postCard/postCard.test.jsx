import { render, screen } from "@testing-library/react";
import { PostCard } from "."

const props = {
    title: "teste 1",
    body: "body 1",
    id: 1,
    cover: "img/img.png"
}

describe("<PostCard />",()=> {
    it("esta na pagina?",()=>{
        render(<PostCard {...props} />);

        expect(screen.getByRole('img')).toBeInTheDocument()
    })
} )