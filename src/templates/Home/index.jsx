import './styles.css';
import { Component, useState, useEffect } from 'react';
import { loadPosts } from '../../utils/load-post';
import { AllPosts } from '../../components/posts';
import { Button } from '../../components/button';
import { TextInput } from '../../components/textInput';

export const Home = () =>{

  const [posts, setPosts ] = useState([])
  const [allPosts, setAllPosts ] = useState([])
  const [page, setPage ] = useState(0)
  const [postsPerPage, setPostsPerPage ] = useState(10)
  const [searchValue, setSearchValue ] = useState("")
  const noMorePosts = page + postsPerPage >= allPosts.length

  const loadCards = async () =>{
    const postsAndPhotos = await loadPosts()
    setPosts(postsAndPhotos.slice(page, postsPerPage))
    setAllPosts(postsAndPhotos)
  }
  
  useEffect(()=>{
    loadCards()
  },[])
  const handleMorePosts = () => {
    const nextPage = page + postsPerPage
    const nextPosts = allPosts.slice(nextPage, nextPage + postsPerPage)
    posts.push(...nextPosts)
    setPage(nextPage)
    setPosts(posts)
  }

  const handleChange = (e) =>{
    const {value} = e.target;
    setSearchValue(value)
  }

  const filteredPosts = !!searchValue ?  posts.filter((post) => {
    return post.title.toLowerCase().includes(searchValue.toLowerCase());
  }) : posts;
  

  return(
    <section className='container'>
      <TextInput searchValue={searchValue} handleChange={handleChange} />
      <AllPosts posts={filteredPosts}  key={posts.id}/>
      {
        !searchValue && <Button text={"Carregar mais posts"} onClick={handleMorePosts} disabledOrNot={noMorePosts} />
      }
    </section>
  )

}



// export class Home2 extends Component{
//     state = {
//       posts: [],
//       allPosts: [],
//       page: 0,
//       postsPerPage:10,
//       searchValue: ""
//     }
    
//     async componentDidMount(){
//       await this.loadCards()
//     }

//     loadCards = async () =>{
//       const {page, postsPerPage} = this.state;

//       const postsAndPhotos = await loadPosts()
//       this.setState({
//         posts: postsAndPhotos.slice(page, postsPerPage),
//         allPosts: postsAndPhotos
//       })
//     }

//     handleMorePosts = () => {
//       const {page, postsPerPage, posts, allPosts} = this.state
//       const nextPage = page + postsPerPage
//       const nextPosts = allPosts.slice(nextPage, nextPage + postsPerPage)
//       posts.push(...nextPosts)

//       this.setState({posts, page:nextPage})
//     }

//     handleChange = (e) =>{
//       const {value} = e.target;
//       this.setState({searchValue: value})
//     }

//     render() {
//       const {posts, page, postsPerPage, allPosts,searchValue  } = this.state
//       const noMorePosts = page + postsPerPage >= allPosts.length

//       const filteredPosts = !!searchValue ?  posts.filter((post) => {
//         return post.title.toLowerCase().includes(searchValue.toLowerCase());
//       }) : posts;
      
//       return(
//         <section className='container'>
//           <TextInput searchValue={searchValue} handleChange={this.handleChange} />
//           <AllPosts posts={filteredPosts}  key={posts.id}/>
//           {
//             !searchValue && <Button text={"Carregar mais posts"} onClick={this.handleMorePosts} disabledOrNot={noMorePosts} />
//           }
//         </section>
//       )
//     }

// }
